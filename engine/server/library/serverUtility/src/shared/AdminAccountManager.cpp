// AdminAccountManager.cpp
// copyright 2002 Sony Online Entertainment

//-----------------------------------------------------------------------

#include "serverUtility/FirstServerUtility.h"
#include "serverUtility/AdminAccountManager.h"

#include "sharedFoundation/ExitChain.h"
#include "sharedUtility/DataTable.h"
#include "sharedUtility/DataTableManager.h"

#include <string>

//-----------------------------------------------------------------------

DataTable * AdminAccountManager::ms_adminTable = 0;
bool        AdminAccountManager::ms_installed = false;
std::string *AdminAccountManager::ms_dataTableName = nullptr;

//-----------------------------------------------------------------------

void AdminAccountManager::install(const std::string &dataTableName)
{
	DEBUG_FATAL(ms_installed, ("AdminAccountManager already installed"));

	ms_adminTable = DataTableManager::getTable (dataTableName, true);
	DEBUG_FATAL(!ms_adminTable, ("Could not open admin file %s!", dataTableName.c_str()));
	ms_installed = true;

	ms_dataTableName = new std::string(dataTableName);

	ExitChain::add(AdminAccountManager::remove, "DataTableManager::remove");
}

//-----------------------------------------------------------------------

void AdminAccountManager::remove()
{
	DEBUG_FATAL(!ms_installed, ("AdminAccountManager not installed"));
	NOT_NULL(ms_dataTableName);
	DataTableManager::close(*ms_dataTableName);
	ms_installed = false;
	delete ms_dataTableName;
	ms_dataTableName = nullptr;
}

//-----------------------------------------------------------------------

const char *AdminAccountManager::getAdminCommandName()
{
	DEBUG_FATAL(!ms_installed, ("AdminAccountManager not installed"));
	return ms_adminTable->getStringValue("AdminSkill", 0);
}

//-----------------------------------------------------------------------

const std::string & AdminAccountManager::getAdminTagName()
{
	DEBUG_FATAL(!ms_installed, ("AdminAccountManager not installed"));
	static const std::string s("*admin*");
	return s;
}

//-----------------------------------------------------------------------

int AdminAccountManager::isAdminAccount(const std::string & account, const std::string & ipAddress)
{
	DEBUG_FATAL(!ms_installed, ("AdminAccountManager not installed"));

	int col = ms_adminTable->findColumnNumber("AdminAccount");
	DEBUG_FATAL(col == -1, ("Error loading admin table...missing account column"));
	int row = ms_adminTable->searchColumnString(col, account);

	if (row == -1/* || !isAllowedLogin(ipAddress)*/)
		return 0;

	return ms_adminTable->getIntValue("AdminLevel", row);
}

//-----------------------------------------------------------------------

bool AdminAccountManager::isAllowedLogin(const std::string & ipAddress)
{
	if (isInternalIp(ipAddress))
		return true;

	int col = ms_adminTable->findColumnNumber("AdminIp");
	DEBUG_FATAL(col == -1, ("Error loading admin table...missing ip column"));
	int row = ms_adminTable->searchColumnString(col, ipAddress);
	return row != -1;
}

//-----------------------------------------------------------------------

bool AdminAccountManager::isInternalIp(const std::string & addr)
{
	DEBUG_FATAL(!ms_installed, ("AdminAccountManager not installed"));
	return addr == "192.168.1.95";
}

//-----------------------------------------------------------------------

void AdminAccountManager::reload()
{
	DEBUG_FATAL(!ms_installed, ("AdminAccountManager not installed"));
	NOT_NULL(ms_dataTableName);
	DataTableManager::reload(*ms_dataTableName);
}

//-----------------------------------------------------------------------
