// ChatConnectAvatar.cpp
// Copyright 2000-02, Sony Online Entertainment Inc., all rights reserved. 
// Author: Justin Randall

//-----------------------------------------------------------------------

#include "serverNetworkMessages/FirstServerNetworkMessages.h"
#include "ChatConnectAvatar.h"

//-----------------------------------------------------------------------

ChatConnectAvatar::ChatConnectAvatar(const std::string & n, const NetworkId & i, const unsigned int s, bool secure) :
GameNetworkMessage("ChatConnectAvatar"),
characterId(i),
characterName(n),
stationId(s),
isSecure(secure)
{
	addVariable(characterId);
	addVariable(characterName);
	addVariable(stationId);
	addVariable(isSecure);
}

//-----------------------------------------------------------------------

ChatConnectAvatar::ChatConnectAvatar(Archive::ReadIterator & source) :
GameNetworkMessage("ChatConnectAvatar"),
characterId(),
characterName(),
stationId(),
isSecure(false)
{
	addVariable(characterId);
	addVariable(characterName);
	addVariable(stationId);
	addVariable(isSecure);
	unpack(source);
}

//-----------------------------------------------------------------------

ChatConnectAvatar::~ChatConnectAvatar()
{
}

//-----------------------------------------------------------------------
